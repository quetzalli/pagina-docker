FROM ubuntu:latest
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y apache2
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
COPY BigWing /var/www/html
CMD ["apachectl", "-D", "FOREGROUND"]